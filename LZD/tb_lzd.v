`timescale 1ns/1ps

module tb_lzd;

parameter N = 8;
parameter W = 3;
reg [N-1 : 0] data;
wire [W-1 : 0] cnt;
wire full;

lzd #(N, W) dut(data, cnt, full);

initial begin
    $dumpfile("wave.vcd");
    $dumpvars;
    #1200
    $finish();
end



initial begin
    data = 8'b00001010;
    #300
    data = 8'b00000101;
    #300
    data = 8'b00010101;
    #300
    data = 8'b00011010;
    #300
    data = 8'b00000000;
end


endmodule