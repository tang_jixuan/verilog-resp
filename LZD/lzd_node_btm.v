/*
 * @Descripttion: 
 * @version: 
 * @Author: charles
 * @Date: 2022-12-06 14:54:20
 * @LastEditors: charles
 * @LastEditorsHomePage: https://gitee.com/tang_jixuan
 * @LastEditTime: 2022-12-06 15:17:29
 */

module lzd_node_btm(left_i, right_i, data_o, cnt_o);

  input          left_i;
  input          right_i;
  output         data_o;
  output         cnt_o;

  assign data_o = left_i || right_i;
  assign cnt_o = left_i ^ data_o;

endmodule
