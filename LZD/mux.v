/*
 * @Descripttion: 
 * @version: 
 * @Author: charles
 * @Date: 2022-12-06 15:39:07
 * @LastEditors: charles
 * @LastEditorsHomePage: https://gitee.com/tang_jixuan
 * @LastEditTime: 2022-12-06 15:40:13
 */

module mux(sel, data1_i, data2_i, data_o);

  parameter N = 2;
  input               sel;
  input     [N-1 : 0] data1_i;
  input     [N-1 : 0] data2_i;
  output    [N-1 : 0] data_o;

  assign data_o = (sel == 1) ? data2_i : data1_i;
  endmodule
