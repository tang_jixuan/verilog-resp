/*
 * @Descripttion: 
 * @version: 
 * @Author: charles
 * @Date: 2022-12-06 14:54:04
 * @LastEditors: charles
 * @LastEditorsHomePage: https://gitee.com/tang_jixuan
 * @LastEditTime: 2022-12-06 15:41:14
 */


module lzd_node(data_i, data_o, cnt_o);
  parameter W = 4;
  parameter N = 2;

  input   [W-1 : 0]                 data_i;
  output                            data_o;
  output  [N-1 : 0]                 cnt_o;

  wire                              left;
  wire                              right;
  wire    [N-2 : 0]                 cnt[1:0];

  generate
    if(N == 1)
    begin
      lzd_node_btm node_btm(data_i[1], data_i[0], data_o, cnt_o[0]);
    end
    else
    begin
      lzd_node #(W>>1, N-1) node[1:0](data_i, {left, right}, {cnt[1], cnt[0]});
      lzd_node_btm node_calc(left, right, data_o, cnt_o[N-1]);
      mux #(N-1) cnt_mux(cnt_o[N-1], cnt[1], cnt[0], cnt_o[N-2 : 0]);
    end
  endgenerate

endmodule
