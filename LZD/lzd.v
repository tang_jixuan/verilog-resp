/*
 * @Descripttion: 
 * @version: 
 * @Author: charles
 * @Date: 2022-12-06 14:52:18
 * @LastEditors: charles
 * @LastEditorsHomePage: https://gitee.com/tang_jixuan
 * @LastEditTime: 2022-12-06 15:43:41
 */
module lzd(data, zcnt, full);

  parameter W = 8;
  parameter N = 3;

  input     [W-1 : 0]     data;
  output    [N-1 : 0]     zcnt;
  output                  full;

  wire full_n;
  assign full = !full_n;
  lzd_node #(W, N) node(data, full_n, zcnt);

endmodule
